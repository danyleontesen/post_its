<?php

use App\Group;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
            'name' => 'Grupo Uno'
        ]);

        Group::create([
            'name' => 'Grupo Dos'
        ]);

        Group::create([
            'name' => 'Grupo Tres'
        ]);

        Group::create([
            'name' => 'Grupo Cuatro'
        ]);

        Group::create([
            'name' => 'Grupo Cinco'
        ]);
    }
}
