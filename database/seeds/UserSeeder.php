<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Usuario Uno',
            'email' => 'usuario1@gmail.com',
            'password' => bcrypt('123456')
        ]);

        User::create([
            'name' => 'Usuario Dos',
            'email' => 'usuario2@gmail.com',
            'password' => bcrypt('123456')
        ]);
    }
}
