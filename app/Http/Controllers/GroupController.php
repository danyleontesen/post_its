<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Resources\GroupResource;
use App\Repositories\GroupRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * GroupController constructor.
     * @param GroupRepository $groupRepository
     */
    public function __construct(GroupRepository $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        #   Mostramos listado de grupos a usuarios autenticados
        return response()->json(['data' => GroupResource::collection($this->groupRepository->all())]);
    }

    /**
     * @param Group $group
     * @return JsonResponse
     */
    public function show(Group $group): JsonResponse
    {
        #   Obtenemos usuario autenticado
        $userId = Auth::user()->getAuthIdentifier();

        #   Verificamos si usuario pertenece al grupo seleccionado
        if (!$this->groupRepository->hasUser($group, $userId)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        #   Mostramos info del grupo seleccionado
        return response()->json(['data' => new GroupResource($group)]);

    }
}
