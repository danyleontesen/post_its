<?php

namespace App\Http\Controllers;

use App\Http\Requests\JoinGroupRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UserRequest $userRequest
     * @return JsonResponse
     */
    public function store(UserRequest $userRequest): JsonResponse
    {
        $data = $userRequest->all();

        #   Guardamos usuario
        $user = $this->userRepository->store($data);

        #   Retornamos información de usuario almacenado
        return response()->json(['data' => $user], 201);
    }

    /**
     * @param User $user
     * @return JsonResponse
     */
    public function show(User $user): JsonResponse
    {
        #   Retornamos información de usuario requerido
        return response()->json(['data' => new UserResource($user)]);
    }

    /**
     * @param JoinGroupRequest $joinGroupRequest
     * @param User $user
     * @return JsonResponse
     */
    public function joinGroup(JoinGroupRequest $joinGroupRequest, User $user): JsonResponse
    {
        //$user = User::find(Auth::user()->getAuthIdentifier());
        $data = $joinGroupRequest->all();

        #   Asignamos grupo al usuario sin que borre a los ya asignados
        $this->userRepository->joinGroup($user, $data['group_id']);

        #   Retornamos informacion del usuario actualizado con sus grupos asignados
        return response()->json(['data' => new UserResource($user)]);
    }
}
