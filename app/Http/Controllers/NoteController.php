<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\Http\Resources\NoteResource;
use App\Jobs\SendEmail;
use App\Mail\NoteCreated;
use App\Note;
use App\Repositories\NoteRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class NoteController extends Controller
{
    /**
     * @var NoteRepository
     */
    protected $noteRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * NoteController constructor.
     * @param NoteRepository $noteRepository
     * @param UserRepository $userRepository
     */
    public function __construct(NoteRepository $noteRepository, UserRepository $userRepository)
    {
        $this->noteRepository = $noteRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param NoteRequest $noteRequest
     * @return JsonResponse
     */
    public function store(NoteRequest $noteRequest): JsonResponse
    {
        $data = $noteRequest->all();

        $data['path'] = "";

        #   Verificamos si cuenta con imagen para guardar
        if ($noteRequest->hasFile('image')) {
            $path = $noteRequest->file('image')->store('notes', 'public');
            $data['path'] = $path;
        }

        #   Creamos la nueva nota
        $note = $this->noteRepository->store($data);

        #   Obtenemos datos del usuario autenticado
        $user = $this->userRepository->findById(Auth::user()->getAuthIdentifier());

        #   Enviamos correo de notificación de nueva nota creada
        SendEmail::dispatch($user->email);

        return response()->json(['data' => new NoteResource($note)], 201);
    }
}
