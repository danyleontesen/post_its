<?php

namespace App\Repositories;

use App\Note;
use Illuminate\Support\Facades\Auth;

class NoteRepository
{
    /**
     * @var Note
     */
    private $note;

    /**
     * NoteRepository constructor.
     */
    public function __construct()
    {
        $this->note = new Note();
    }

    public function store(array $data)
    {
        return $this->note::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'path' => $data['path'],
            'group_id' => $data['group_id'],
            'user_id' => Auth::user()->getAuthIdentifier(),
        ]);
    }
}
