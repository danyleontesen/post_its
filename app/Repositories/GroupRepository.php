<?php

namespace App\Repositories;

use App\Group;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GroupRepository
{
    /**
     * @var Group
     */
    private $group;

    /**
     * GroupRepository constructor.
     */
    public function __construct()
    {
        $this->group = new Group();
    }

    /**
     * @return Group[]|Collection
     */
    public function all()
    {
        return $this->group::all();
    }

    /**
     * @param Group $group
     * @param int $userId
     * @return bool
     */
    public function hasUser(Group $group, int $userId): bool
    {
        return $group->users()->where('user_id', $userId)->exists();
    }
}
