<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->user = new User();
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function findById(int $userId)
    {
        return $this->user::find($userId);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->user::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * @param User $user
     * @param int $groupId
     */
    public function joinGroup(User $user, int $groupId)
    {
        $user->groups()->syncWithoutDetaching([$groupId]);
    }
}
