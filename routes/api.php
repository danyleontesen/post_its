<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/**
 *  Los usuarios pueden registrarse y crear una cuenta nueva
 */
Route::post('/users', 'UserController@store');


Route::middleware('auth:api')->group(function () {

    /*
     *  Al iniciar sesión puedo ver los grupos que existen y puedo unirme a uno.
     */
    Route::get('/groups', 'GroupController@index');

    /**
     *  Los usuarios del grupo pueden ver el contenido del grupo
     */
    Route::get('/groups/{group}', 'GroupController@show');

    /**
     *  Mostrar detalles de usuario
     */
    Route::get('/users/{user}', 'UserController@show');

    /**
     *  Un usuario puede unirse a grupos existentes
     */
    Route::post('/users/{user}/join-group', 'UserController@joinGroup');

    /**
     *  Guardar una nota y notificar a los usuarios del grupo que se ha creado una nueva nota
     */
    Route::post('/notes', 'NoteController@store');

});
